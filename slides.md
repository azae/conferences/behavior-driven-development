# Vous saurez tout sur le BDD

::: notes
Nous sommes très prétentieux, tout le BDD en 60 minutes ça risque d'aller très vite. Notre objectif survoler le sujet et vous donner envie de creuser chaque détail.

Nous sommes friant de retours d'expérience, vous trouverez nos contact en fin de conférence.
:::

#
<table>
  <tr>
    <td>Thomas Clavier</td>
    <td><img src="./includes/thomas-clavier.jpg" style="width: 160px"></td>
  </tr>
</table>


# C'est l'histoire de 3 Amis

Le truc qui part en prod c'est ce que le dev à compris.

::: notes
QA, BIZ, DEV collaborer pour obtenir un produit.

Les gens qui disent faire du BDD tout seul dans leur coin ne font pas du BDD.
:::

# Pourquoi ?

* Se comprendre
* Réduire le gâchis
* Guider le développement du produit

# Se comprendre

Est-on certain d'avoir compris la même chose ?

::: notes
feuilleté de bœuf sichuanais.

En binôme, racontez une histoire dans laquelle on aurait put demander à chaqu'un : Vous avez bien compris ? Oui ?
:::

# {data-background-image="./includes/canard-lapin.png" }

::: notes
Vous voyez quoi ? langage de description qui pourrait être le même ... comment lever l'ambigüité ?

Imaginez, vous n'avez jamais vu de lapin. Dire « Il y a bien un second aspect dans cette image, mais je ne saurais dire ce que c'est » est un non-sens
:::

# Réduire le gâchis

* Rework = incompréhension du dev et/ou interprétation du dev
* Influence du business sur le code

::: notes
* Rework : travaux de rénovation de mon mur en brique.  On appel rework, le fait de refaire une fonctionnalité plusieurs fois.
* Rework : Check list, le même texte qui décrit l'US, le PO et le dev n'en avait pas la même interprétation.
* Influence du dev : le client change d'avis toutes les 5 min => adapter son archi aux changements du clients
:::

# Guider le développement

* Architecture émergente
* Adaptation au marché
* Cohésion avec la qualité

::: notes

* Pour le dev :
  * visibilité = sérénité
  * architecture émergente
  * Dan Pink : Why + autonomie = motivation
* business : livraison de petits incrément = pivot facile sans avoir besoins de faire des études. Amazon = 180000 features par jour, moins de 1000 après 24h
* qa : implication au plus tôt ce ne sont pas des emerdeurs
:::

# Le BDD tous les jours

Processus itératif en 3 étapes.

# Étapes
![](./includes/bdd-practices-diagram.png){height=500px}

::: notes

* Prendre un petit incrément, une nouvelle fonctionnalité, discuter, discuter avec des exemples, se mettre d'accord, s'accorder sur ce que l'on dit.
* Documenter ce que l'on vient de se dire
* Finalement l'automatiser pour guider le code.
:::


# Atelier de découverte

Ce que le logiciel pourrait faire.


::: notes
Le BDD permet d'avoir la bonne conversation au bon moment afin de maximiser la valeur du code que vous allez produire.

Des conversations constructives

Nous utilisons des conversations structurées, appelées ateliers de découverte, qui se concentrent sur des exemples réels du système du point de vue des utilisateurs. Ces conversations permettent à notre équipe de mieux comprendre les besoins des utilisateurs, les règles qui régissent le fonctionnement du système et l'étendue de ce qui doit être fait.

Elles peuvent également révéler des lacunes dans notre compréhension, où nous avons besoin de plus d'informations avant de savoir ce qu'il faut faire.

L'examen d'une session de découverte révèle souvent des fonctionnalités de faible priorité qui peuvent être écartées de la portée d'une histoire d'utilisateur, ce qui aide l'équipe à travailler par petites étapes, améliorant ainsi leur flux.

Si vous êtes nouveau dans le BDD, la découverte est le bon endroit pour commencer. Les deux autres pratiques ne vous apporteront pas beaucoup de joie tant que vous n'aurez pas maîtrisé la découverte.
:::

# Exemples mapping
![](./includes/exemple-mapping.png)


# Entrainement

::: notes
Vous avez feuille et crayon, essayez de faire une cartographie de l'histoire : une machine à café, compter les points au bowling, etc.
:::

# Formulation

Ce que le logiciel devrait faire.

::: notes
Dès que nous avons identifié au moins un exemple précieux lors de nos séances de découverte, nous pouvons maintenant formuler chaque exemple sous forme de documentation structurée. Cela nous donne un moyen rapide de confirmer que nous avons vraiment une compréhension commune de ce qu'il faut construire.

Contrairement à la documentation traditionnelle, nous utilisons un support qui peut être lu à la fois par les humains et les ordinateurs, de sorte que :

* Nous pouvons obtenir un retour d'information de toute l'équipe sur notre vision commune de ce que nous construisons.
* Nous pourrons automatiser ces exemples pour guider notre développement de la mise en œuvre.

En écrivant cette spécification exécutable de manière collaborative, nous établissons un langage partagé pour parler du système. Cela nous aide à utiliser la terminologie du domaine des problèmes jusqu'au code.

:::

# Automatisation

Ce que le logiciel fait réellement.

::: notes
Maintenant que nous avons notre spécification exécutable, nous pouvons l'utiliser pour guider notre développement de la mise en œuvre.

Prenons un exemple à la fois, nous l'automatisons en le connectant au système à titre de test. Le test échoue parce que nous n'avons pas encore implémenté le comportement qu'il décrit. Nous développons maintenant le code d'implémentation, en utilisant des exemples de niveau inférieur du comportement des composants internes du système pour nous guider si nécessaire.

Les exemples automatisés fonctionnent comme des rails de guidage, ce qui nous aide à maintenir notre travail de développement sur la bonne voie.

Lorsque nous devrons revenir et maintenir le système plus tard, les exemples automatisés nous aideront à comprendre ce que le système fait actuellement, et à apporter des modifications en toute sécurité sans rien casser involontairement.

Ce retour d'information rapide et reproductible réduit la charge des tests de régression manuels, libérant ainsi les gens pour effectuer des travaux plus intéressants, comme les tests exploratoires.
:::


# Outils

* Personas
* Exemples
* Ubiquitous language
* Gherkin


# Personas

* Lever une ambiguité et se projetter dans des usages différents
* Fluidifier la conversation du business vers les développeurs et les utilisateurs
* Élaborer la stratégie produit pour le business

::: notes
Personnifier une catégorie d'utilisateurs à travers 1 personnage, nommé, avec des caractéristiques précises. Partagé entre nos 3 amis.

Permet d'éviter un "périparagraphe" et les répétitions de contexte.

plateforme GTS
 * Support GUI :
 * Paris OPS API automatisable && auditable
:::

# Exemples

::: notes
Comme vous avez vu, thomas n'arrête pas d'illustrer les concepts avec des exemples. L'exemple est important, mais voyons ça en détail.
:::

# Règle sans exemple

::: incremental
* Comment faire un nœud de chaise ?
* Dessinez une étoile à 12 points
:::

# Étoile à 12 points

![](./includes/etoiles.svg)


# Exemple sans règle

|<span style="color:red">KO</span>|<span style="color:green">OK</span> |
|----|----|
|<span style="color:red">thieGh6E</span>|<span style="color:green"> Di2ceik5D</span> |
|<span style="color:red">azerTy007</span>|<span style="color:green">aY0wae1oo</span> |
|<span style="color:red">kexuCacee</span>|<span style="color:green">somuuS4ge</span> |

# Exemple sans règle

|<span style="color:red">KO</span>|<span style="color:green">OK</span> | Règle |
|----|----|----|
|<span style="color:red">thieGh6E</span>|<span style="color:green"> Di2ceik5D</span> | 9 charactères
|<span style="color:red">azerTy007</span>|<span style="color:green">aY0wae1oo</span> | pas dans le dico
|<span style="color:red">kexuCacee</span>|<span style="color:green">somuuS4ge</span> | des chiffres et des lettres

# Language Commun co-construit
Ubiquitous Language

::: notes
Pour se comprendre c'est quand même plus facile si on parle la même langue.

vient du DDD, vient répondre au problème d'incompréhension dans la même langue. C'est le nom de ce langage commun co-construit.
:::

# Dans les exemples

- Changer les critères de segmentation sur la famille lave linge
- Changer les attributs du noeud de nomenclature de niveau 6

# Dans les exemples

Des mots polysémiques.

::: incremental
- Dans le domaine des vegétaux la tomate est un fruit
- Dans le domaine de la cuisine, la tomate est un légume
- Dans le domaine de l'école, la tomate est un jeu
- Dans le domaine du spectacle, la tomate est un feedback
:::

# Dans le code

Imaginez un distributeur avec :

- une nomenclature de référencement
- une nomenclature de vente

::: notes
- une nomenclature de référencement dans laquelle, le rayon papeterie regroupe toutes les familles de produits en papier (papier, filtre à café, mouchoir, etc.)
- une nomenclature de vente dans laquelle le rayon papeterie parle de matériel pour le bureau
:::

# Gherkin

::: notes
C'est un langage fait pour être lu facilement par une personne non technique.

Le but de Gherkin est de promouvoir le BDD dans une équipe de développement en incluant les 3 amis.
:::

# Scenario = un exemple

![](./includes/scenario.png)

# Feature = une fonctionnalité

![](./includes/feature.png)

::: notes
À vous, notez les fonctionnalités et les scenarios du bowling que vous avez cartographiés
:::

# Français

![](./includes/french.png)

# Automatisable

![](./includes/automation.png)

# UX / UI

::: notes
L'UX influence grandement l'UI. L'UI fait parti de l'UX.
C'est difficile de trouver une délimitation claire entre les deux.
:::

# 1955
![](./includes/sk8-1955.png)

# 2015
![](./includes/sk8-2015.png)

# Durée de vie des tests

* **Quand** je m'identifi.
* **Quand** je rentre mon login, mon mot de passe et que je clic sur valider.

::: notes
L'UX est beaucoup plus durable que l'UI :
:::

# {data-background-image="./includes/takeaway.jpg" data-state="white80"}

 * 3 étapes (Découvrir, Formaliser, Automatiser)
 * Des outils (Exemples, Personas, Exemple Mapping, Ubiquitous language, Gherkin)
 * Pour (se comprende, réduire le gachi, guider le développement)


# Partagez vos expériences {data-background-image="./includes/logo-azae.svg" data-state="white90"}

<table >
  <tr>
    <td><img src="./includes/thomas-clavier.jpg" style="width: 120px"></td>
    <td>
      <i class="fa fa-twitter"></i> @thomasclavier <br/>
      <i class="fa fa-mastodon"></i> @thomas@libre-entreprise.com <br/>
    </td>
  </tr>
  <tr>
    <td class="icon"><i class="fa fa-globe"></i></td>
    <td>https://ajiro.fr/talks/conference-bdd/</td>
  </tr>
</table>

::: notes

* Nous espérons vous avoir donné envie de creuser le sujet
* Nous sommes friant de belles histoires
* N'hésitez pas à partager avec nous vos retours d'expériences.

:::
