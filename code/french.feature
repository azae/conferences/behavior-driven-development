# language: fr
Fonctionnalité: Exemple de fonctionalité
Afin d'essayer le BDD sur le kata Bowling
En tant que développeur
Je souhaite avoir un exemple

Scénario: Je ne fais pas tomber de quille
Étant donné une partie de Bowling
Quand toutes mes balles vont dans la goutière
Alors mon score total est de 0

Scénario: Je marque mon premier point
Soit une partie de Bowling avec un score de 0 point
Quand ma balle renverse une quille
Alors mon score total est de 1
